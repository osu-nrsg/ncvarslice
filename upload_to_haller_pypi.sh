#!/bin/bash

REMOTE_DIR=/nfs/depot/cce_u1/haller/shared/public_html/pypi/ncvarslice/

rsync -rtP dist/ Access:$REMOTE_DIR
ssh Access chmod -vR a+rX $REMOTE_DIR
