# ncvarslice - NetCDF variable slicer

## Introduction

In NetCDF schemas such as the
[CFRadial Format](https://raw.githubusercontent.com/NCAR/CfRadial/master/old_docs/CfRadialDoc.v1.3.20130701.pdf),
one or more virtual dimensions may exist that subdivide a real dimension. In this situation, ancillary
variables must be provided to specify the start and stop indices in the real dimension that are the
boundaries of each unit in the virtual dimension.

In the case of the CFRadial format the _sweep_ dimension subdivides the _time_ dimension, and the
_sweep\_start\_ray\_index_ and _sweep\_end\_ray\_index_ variables specify the start and end boundaries, in
the _time_ dimension, of each sweep.

Once created, an NCVarSlicer object may be called with `__getitem__` (i.e. `[]`) syntax. If a single integer
index is provided, a single slice object is returned corresponding to the real dimension bounds of that index
in the virtual dimension. If a slice is provided, then a list of slice objects is returned corresponding to
each selected index in the virtual dimension.

## Example Usage

Given:

- A netCDF file `dataset.nc` with timeseries data
  - Dimensions `time` and `sample_group`
  - Data variable `intensity(time)`
  - Group indexing variables `group_start_index(sample_group)` and `group_end_index(sample_group)`

```python
import netCDF4
from ncvarslice import NCVarSlicer

with netCDF4.Dataset("dataset.nc") as ds:
    group_slicer = NCVarSlicer(ds["group_start_index"], ds["group_end_index"])
    for slc in group_slicer:
        # Work with each sample group
        sample_group_data = ds["intensity"][slc]

```

Equivalent to:

```python
import netCDF4

with netCDF4.Dataset("dataset.nc") as ds:
    for i in range(ds.dimensions["sample_group"].size):
        a = ds["group_start_index"][i]
        b = ds["group_end_index"][i] + 1
        # Work with each sample group
        sample_group_data = ds["intensity"][a:b]

```

### CfRadial Example

```python
import netCDF4
from ncvarslice import NCVarSlicer

with netCDF.Dataset("dataset.nc") as ds:
  for slc in NCVarSlicer(ds["sweep_start_ray_index"], ds["sweep_end_ray_index"]):
    # slc is a slice object providing the indices of the time dimension for a single radar sweep
    sweep_power = ds["PWR"][slc][:]  # power values for all the rays in the sweep (at every range)
    sweep_times = ds["time"][slc]  # timestamps of the rays in the sweep
    sweep_azimuths = ds["azimuth"][slc]  # azimuths of the rays in the sweep

  # Or, to process groups of sweeps:
  sweeps_per_group = 4
  sweep_slicer = NCVarSlicer(ds["sweep_start_ray_index"], ds["sweep_end_ray_index"])
  for slc_i in range(0, len(varslicer), sweeps_per_group):
    sweep_slices = sweep_slicer[slc_i : slc_i + sweeps_per_group]
    ...  # sweep_slices is a list of slices for each sweep in the group
    ...  # perhaps do parallel processing on four sweeps at a time
```
