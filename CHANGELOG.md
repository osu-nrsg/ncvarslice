# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Updated README.

## [1.1.2]

- Added __iter__ to be officially Iterable.

## [1.1.1]

- Added `@typing.overload` to `__getitem__`.

## [1.1.0]

- Added `offline` options (default True) to get slices at init time rather than at `__getitem__` time.

## [1.0.0]

- Add license and add example to README

## [0.3.0]

- Use [single-version](https://github.com/hongquan/single-version)  for `__version__`
- Rename `VarSlice` to `NCVarSlicer` (`VarSlice` still works)

## [0.2.0]

### Added

- Give a helpful error if the dataset isn't open

### Modified

- Rename module _varslice.py to varslice.py (no need to be hidden)

## [0.1.0]

First release.

[Unreleased]: https://gitlab.com/osu-nrsg/ncvarslice/-/compare/v1.1.2...master
[1.1.2]: https://gitlab.com/osu-nrsg/ncvarslice/-/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitlab.com/osu-nrsg/ncvarslice/-/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/osu-nrsg/ncvarslice/-/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/osu-nrsg/ncvarslice/-/compare/v0.3.0...v1.0.0
[0.3.0]: https://gitlab.com/osu-nrsg/ncvarslice/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/osu-nrsg/ncvarslice/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/osu-nrsg/ncvarslice/-/tags/v0.1.0
